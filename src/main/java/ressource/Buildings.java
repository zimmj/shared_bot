package ressource;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import ogame.bot.logic.NextBot;

public enum Buildings {
	MetalMine(1, "Metal Mine"), CrystalMine(2, "Crystal Mine"), DeuteriumRefinery(3, "Deuterium Refinery"), SolarPower(
			4, "Solar"), RobotFactory(14, "Robot"), NaniteFactory(15, "Nanite"), Shipyard(21, "Ship"), MetalStorage(22,
					"Metal Storage"), CrystalStorgae(23, "Crystal Storage"), DeuteriumStorage(24,
							"Deuterium Storage"), ResearchLab(31, "Research Lab"), Terraformer(33,
									"Terra"), AllianceDepot(34, "Alliance"), MissleSilo(44, "Missle");

	private Buildings(int number, String name) {
		this.number = number;
		this.name = name;
	}

	private String name;
	private int number;

	public String getName() {
		return name;
	}

	public int getNumber() {
		return number;
	}

	public final static String buildingsLink = "page=buildings";
	public final static String planetSuffix = "&mode=&cp=";

	public static void goToBuildingPage(WebDriver webdriver, Integer planetNumber) {
		if (!webdriver.getCurrentUrl().contains(buildingsLink)) {
			System.err.println("Wrong Page: " + webdriver.getCurrentUrl());
			if (planetNumber == 0) {
				webdriver.get(NextBot.game_url + buildingsLink);
				System.err.println(webdriver.getCurrentUrl());
			} else
				webdriver.get(NextBot.game_url + buildingsLink + planetSuffix + planetNumber);
		}
	}

	public boolean build(WebDriver webDriver, Integer planetNumber) {
		goToBuildingPage(webDriver, planetNumber);
		try {

			webDriver.findElement(By.id("build" + number)).sendKeys("1");
			webDriver.findElement(By.className("build_submit")).click();
			return true;
		} catch (Exception e) {
			System.err.println(e.getMessage());
			return false;
		}
	}

	public static Integer getBuildingLevel(Buildings building, WebDriver webDriver, int planetNumber) {
		goToBuildingPage(webDriver, planetNumber);
		try {
			String[] infos = webDriver.findElement(By.xpath("//td[contains(.,'" + building.getName() + "')]  "))
					.getText().split("\n");
			List<String> input = Arrays.asList(infos);
			List<String> levels = input.stream().filter(s -> s.contains("Level"))
					.map(s -> s.substring(s.indexOf("Level ") + 6, s.indexOf(")", s.indexOf("Level"))))
					.collect(Collectors.toList());
			return Integer.valueOf(levels.get(0));
		} catch (Exception e) {
			System.err.println("Non Level found " + e.getMessage());
			return 0;
		}

	}

	public final BigInteger[] getCost(WebDriver webDriver) {
		if (webDriver.getCurrentUrl().contains(buildingsLink)) {
			BigInteger metalCost;
			BigInteger crystalCost;
			BigInteger deuteriumCost;
			try {
				metalCost = new BigInteger(webDriver.findElement(By.id("metal_" + number)).getText());
			} catch (Exception e) {
				metalCost = new BigInteger("0");
			}
			try {
				crystalCost = new BigInteger(webDriver.findElement(By.id("crystal_" + number)).getText());
			} catch (Exception e) {
				crystalCost = new BigInteger("0");
			}
			try {
				deuteriumCost = new BigInteger(
						webDriver.findElement(By.id("deuterium_" + number)).getText());
			} catch (Exception e) {
				deuteriumCost = new BigInteger("0");
			}
			return new BigInteger[] { metalCost, crystalCost, deuteriumCost };

		}
		return new BigInteger[] { new BigInteger("-1"), new BigInteger("-1"), new BigInteger("-1") };
	}
}
