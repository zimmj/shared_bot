package ressource.planets.subTask;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;

import ressource.Buildings;
import ressource.Ressource;

public class BuildMines implements Task {

	private WebDriver webDriver;
	private Integer planetNumber;
	Map<String, MineBuilding> planetMines = new HashMap<String, MineBuilding>();

	public BuildMines(WebDriver webDriver, Integer planetNumber) {
		this.webDriver = webDriver;
		this.planetNumber = planetNumber;
		initBuildingsLevel();
	}

	private void initBuildingsLevel() {
		planetMines.put(Buildings.MetalMine.getName(), new MineBuilding(Buildings.MetalMine)); // 1
		planetMines.put(Buildings.CrystalMine.getName(), new MineBuilding(Buildings.CrystalMine));// 1
		planetMines.put(Buildings.DeuteriumRefinery.getName(), new MineBuilding(Buildings.DeuteriumRefinery));// 0.5
		planetMines.put(Buildings.SolarPower.getName(), new MineBuilding(Buildings.SolarPower));
	}

	private class MineBuilding {
		private Buildings type;
		private Integer level;

		protected MineBuilding(Buildings name) {
			this.type = name;
			this.level = Buildings.getBuildingLevel(name, webDriver, planetNumber);
		}

		protected Buildings getName() {
			return type;
		}

		protected Integer getLevel() {
			return level;
		}

		protected void increaseLevel() {
			level++;
		}
	}

	@Override
	public void startTask(WebDriver webDriver) {
		Buildings toBuild = toBuild();
		BigInteger[] costs=toBuild.getCost(webDriver);
		System.out.println(toBuild.getName());
		while(!checkCost(costs)) {}
		toBuild.build(webDriver, planetNumber);

	}
	
	private boolean checkCost(BigInteger[] costs) {
		if (Ressource.Metal.getValue(webDriver).compareTo(costs[0])==-1) {
			return false;
		}
		if (Ressource.Crystal.getValue(webDriver).compareTo(costs[1])==-1) {
			return false;
		}
		if (Ressource.Deuterium.getValue(webDriver).compareTo(costs[2])==-1) {
			return false;
		}
		else return true;
	}

	private Buildings toBuild() {
		if (!Ressource.Energy.getEnergy(webDriver)) {
			return Buildings.SolarPower;
		}
		Integer metalLevel = planetMines.get(Buildings.MetalMine.getName()).getLevel();
		Integer crystaRatio = planetMines.get(Buildings.CrystalMine.getName()).getLevel() / metalLevel;
		Integer deuteriumRatio = planetMines.get(Buildings.DeuteriumRefinery.getName()).getLevel() / metalLevel;
		if (crystaRatio < 0.8) {
			return Buildings.CrystalMine;
		}
		if (deuteriumRatio < 0.5) {
			return Buildings.DeuteriumRefinery;
		}
		return Buildings.MetalMine;
	}

}
