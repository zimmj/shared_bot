package ressource;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public enum Research {
	Spy("Spy Technology"), Computer("Computer Technology"), Weapons("Weapons Technology"), Shield("Shield Technology"),
	Armour("Armour Technology"), Energy("Energy Technology"), HyperspaceTech("Hyperspace  Technology"), Combustion("Combustion Engine"),
	Impulse("Impulse Engine"), HyperspaceEn("Hyperspace Engine"), Laser("Laser Technology"),
	Ion("Ion Technology"), Plasma0("Plasma Technology"), Intergalactic("Intergalactic Research"), Expedition("Expedition Research"),
	Metal("Improved Metal"), Crystal("Improved Crystal"), Deuterium("Improved Deuterium"),Graviton("Graviton Research");
	
	private String researchName;
	private Research(String name) {
		researchName = name;
	}
	
	private String getName() {
		return researchName;
	}
	
	public final static String researchLink = "page=buildings&mode=research&cp=";
	
	public final static void research(Research research, WebDriver webDriver) {
		if(webDriver.getCurrentUrl().contains(researchLink)) {
			try {
				webDriver.findElement(By.xpath("//*[text()='"+research.getName()+"']"));
				try {
					webDriver.findElement(By.className("build_submit")).click();
				} catch (Exception e) {
					System.err.println("Button not found");
				}
			}catch (Exception e) {
				System.err.println("Forchung nicht gefunden");
			}
		}
		
		
	}
}
