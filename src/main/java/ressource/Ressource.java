package ressource;

import java.math.BigInteger;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public enum Ressource {
	Metal("current_metal"), Crystal("current_crystal"), Deuterium("current_deuterium"), Energy("Energy");

	private String name;

	private Ressource(String name) {
		this.name = name;
	}

	private static Boolean parseEnergy(String enegery) {
		if (enegery.substring(0, 1).equals("-")) {
			return false;
		}
		return true;
	}

	private static BigInteger parseRessourceNumber(String number) {
		int numberLength = number.length();
		String suffix = number.substring(numberLength - 1, numberLength);
		number = number.substring(0, numberLength - 1).replace(".", "").trim();
		switch (suffix) {
		case "T":
			return numberFactor.T.transform(number);
		case "Q":
			return numberFactor.Q.transform(number);
		default:
			return new BigInteger(number);
		}
	}

	public BigInteger getValue(WebDriver webDriver) throws NullPointerException {
		try {
			WebElement ressourceElement = webDriver.findElement(By.id(name));
			return parseRessourceNumber(ressourceElement.getText());
		} catch (Exception e) {
			System.err.println("Gesuchte Ressource nicht gefunden");
			throw new NullPointerException();
		}
	}

	public boolean getEnergy(WebDriver webDriver) throws NullPointerException {
		try {
			WebElement energyLabel = webDriver.findElement(By.xpath("//*[text()='Energy']"));
			WebElement energyElement = energyLabel.findElement(By.xpath("following-sibling::*[1]"));
			return parseEnergy(energyElement.getText());
		} catch (Exception e) {
			System.err.println("Energy nicht gefunden");
			throw new NullPointerException();
		}
	}

	private enum numberFactor {
		T(1e18), Q(1e30);

		private numberFactor(double doubleFactor) {
			this.bigFactor = BigInteger.valueOf((long) doubleFactor);
		}

		private final BigInteger bigFactor;

		protected BigInteger getFaktor() {
			return bigFactor;
		}

		protected BigInteger transform(String number) {
			number = number.replace(".", "");
			BigInteger out = BigInteger.valueOf(Long.parseLong(number));
			return out.multiply(bigFactor);
		}
	}
}
