package ogame.bot.logic;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {
	
	private final String ogameStart="https://de.ogame.gameforge.com/";
	
	enum Browser {
		CHROME, IE
	}

	private WebDriver webDriver;
	
	public HomePage() {}

	public HomePage(Browser browser) throws URISyntaxException {
		setProperties();
		switch (browser) {
		case CHROME:
			webDriver = new ChromeDriver();
			break;
		case IE:
			webDriver = new InternetExplorerDriver();
			break;
		}
	}
	
	public String getClasspathString() {
	     StringBuffer classpath = new StringBuffer();
	     ClassLoader applicationClassLoader = this.getClass().getClassLoader();
	     if (applicationClassLoader == null) {
	         applicationClassLoader = ClassLoader.getSystemClassLoader();
	     }
	     URL[] urls = ((URLClassLoader)applicationClassLoader).getURLs();
	      for(int i=0; i < urls.length; i++) {
	          classpath.append(urls[i].getFile()).append("\r\n");
	      }    
	     
	      return classpath.toString();
	  }

	private void setProperties() throws URISyntaxException {
		URL chromeURL = getClass().getClassLoader().getResource("driver" + File.separator + "chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", Paths.get(chromeURL.toURI()).toFile().getAbsolutePath());

//		URL firefoxURL = geetClass().getClassLoader().getResource("driver" + File.separator + "geckodriver.exe");
//		System.setProperty("webdriver.gecko.driver", Paths.get(firefoxURL.toURI()).toFile().getAbsolutePath());
//
		URL ieURL = getClass().getClassLoader().getResource("driver" + File.separator + "IEDriverServer.exe");
		System.setProperty("webdriver.ie.driver", Paths.get(ieURL.toURI()).toFile().getAbsolutePath());
	}

	public void goToGoogleAndSearch() {
		webDriver.get(ogameStart+"/#login");
		WebElement coseButtpn=webDriver.findElement(By.className("openX_int_closeButton"));
		coseButtpn.click();
		WebDriverWait wait2 = new WebDriverWait(webDriver, 10);
		wait2.until(ExpectedConditions.invisibilityOfElementLocated(By.id("ui-id-1")));
		WebElement login = webDriver.findElement(By.id("ui-id-1"));
		login.click();
		
		wait2.until(ExpectedConditions.invisibilityOfElementLocated(By.id("usernameLogin")));
		WebElement searchFieldElementMail = webDriver.findElement(By.id("usernameLogin"));
		searchFieldElementMail.sendKeys("zimmi.94@live.de");
		WebElement searchFieldElementPass = webDriver.findElement(By.id("passwordLogin"));
		searchFieldElementPass.sendKeys("aumat0");
		searchFieldElementPass.sendKeys(Keys.ENTER);
	}

	public void quit() {
		try {
		TimeUnit.SECONDS.sleep(10);
		webDriver.quit();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			webDriver.quit();
		}
	}

	public static void main(String[] args) throws URISyntaxException {
		
        HomePage seleniumTutorial = new HomePage(Browser.CHROME);
//        System.out.println(seleniumTutorial.getClasspathString());
        seleniumTutorial.goToGoogleAndSearch();
        seleniumTutorial.quit();
    }
}
