package ogame.bot.logic;



import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import ogame.bot.BotEnviro;
import ogame.bot.BotEnviro.Browser;
import ressource.Buildings;
import ressource.Research;
import ressource.planets.MainPlanet;
import ressource.planets.subTask.BuildMines;




public class NextBot {
	private final String MAIN_URL="http://corrado-game.org/";
	private final String LOGIN_SUF="login1.php";
	public  final static String universe="uni-11";
	public  final static String game_url="http://"+universe+".corrado-game.org/game.php?";
	
	private WebDriver ogameDriver;
	private BotEnviro botEnviro;
	
	public NextBot(Browser browser) {
		botEnviro = new BotEnviro(browser);
		ogameDriver = botEnviro.getDriver();
		
		
	}
	
	public void login() {
		ogameDriver.get(MAIN_URL+LOGIN_SUF);
		WebElement uni = ogameDriver.findElement(By.id("universe2"));
		uni.sendKeys(Keys.DOWN);//, Keys.DOWN, Keys.DOWN, Keys.DOWN);
		
		WebElement user = ogameDriver.findElement(By.id("username"));
		user.sendKeys("siedler");
		WebElement pwd = ogameDriver.findElement(By.id("password"));
		pwd.sendKeys("aumat0");
		WebElement submit = ogameDriver.findElement(By.name("Submit"));
		submit.submit();	
	}
	
	
	public void tryBuild() {
		String building_URL=game_url+ressource.Buildings.buildingsLink+1749;
		System.out.println(building_URL);
		ogameDriver.get(building_URL);
		Buildings.MetalMine.build(ogameDriver, 0);
	}
	
	public void tryResearch() {
		String research_URL=game_url+Research.researchLink+113;
		System.out.println(research_URL);
		ogameDriver.get(research_URL);
		Research.research(Research.Spy, ogameDriver);
	}
	
	public void tryFindLvl() {
		System.out.println(Buildings.getBuildingLevel(Buildings.ResearchLab, ogameDriver, 0));
	}
	
	public void buildMines() {
		BuildMines builder = new BuildMines(ogameDriver, 0);
		builder.startTask(ogameDriver);
		builder.startTask(ogameDriver);
		builder.startTask(ogameDriver);
	}
	
	
	public static void main(String[] args) {
		NextBot ogame = new NextBot(Browser.CHROME);
		ogame.login();
//		ogame.tryBuild();
//		ogame.tryResearch();
//		ogame.tryFindLvl();
		ogame.buildMines();
	}
}
