package ogame.bot;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import ressource.Ressource;


public class BotEnviro {
	public enum Browser {
		CHROME, IE
	}
	public enum DriverCondition{
		NonRessource
	}

	private final WebDriver driver;
		
	public BotEnviro(Browser browser) {
		try {
			setProperties();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		switch (browser) {
		case CHROME:
			driver = new ChromeDriver();
			break;
		case IE:
			driver = new InternetExplorerDriver();
			break;
		default:
			driver = new ChromeDriver();
		}
		
	}
	
	private void setProperties() throws URISyntaxException {
		URL chromeURL = getClass().getClassLoader().getResource("driver" + File.separator + "chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", Paths.get(chromeURL.toURI()).toFile().getAbsolutePath());

//		URL firefoxURL = geetClass().getClassLoader().getResource("driver" + File.separator + "geckodriver.exe");
//		System.setProperty("webdriver.gecko.driver", Paths.get(firefoxURL.toURI()).toFile().getAbsolutePath());
//
		URL ieURL = getClass().getClassLoader().getResource("driver" + File.separator + "IEDriverServer.exe");
		System.setProperty("webdriver.ie.driver", Paths.get(ieURL.toURI()).toFile().getAbsolutePath());
	}

	public WebDriver getDriver() {
		return driver;
	}

}
